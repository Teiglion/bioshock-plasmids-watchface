#include <pebble.h>
Window *window;
TextLayer *text_logo;
TextLayer *textHours;
TextLayer *textMinutes;
BitmapLayer *Image;
GBitmap *Icon;
BitmapLayer *subImage;
GBitmap *subIcon;
AppTimer *timer;
bool timestamp=true;

//the list of resources for shaking "color"
static const uint32_t images[]={RESOURCE_ID_insectwarm_icon,RESOURCE_ID_target_dummy_icon,RESOURCE_ID_winter_blast_icon,RESOURCE_ID_telekinesis_icon,RESOURCE_ID_security_bullseye_icon,RESOURCE_ID_enrage_icon,RESOURCE_ID_cyclonetrap_icon,RESOURCE_ID_electrobolt_icon,RESOURCE_ID_incenerate_icon,RESOURCE_ID_befriend_icon};

//the list of resources for shaking "round"
static const uint32_t images_rnd[]={RESOURCE_ID_insectwarm_icon_rd,RESOURCE_ID_target_dummy_icon_rd,RESOURCE_ID_winter_blast_icon_rd,RESOURCE_ID_telekinesis_icon_rd,RESOURCE_ID_security_bullseye_icon_rd,RESOURCE_ID_enrage_icon_rd,RESOURCE_ID_cyclonetrap_icon_rd,RESOURCE_ID_electrobolt_icon_rd,RESOURCE_ID_incenerate_icon_rd,RESOURCE_ID_befriend_icon_rd};


//the list of resources for shaking "black"
static const uint32_t images_blk[]={RESOURCE_ID_insectwarm_icon_blk,RESOURCE_ID_target_dummy_icon_blk,RESOURCE_ID_winter_blast_icon_blk,RESOURCE_ID_telekinesis_icon_blk,RESOURCE_ID_security_bullseye_icon_blk,RESOURCE_ID_enrage_icon_blk,RESOURCE_ID_cyclonetrap_icon_blk,RESOURCE_ID_electrobolt_icon_blk,RESOURCE_ID_incenerate_icon_blk,RESOURCE_ID_befriend_icon_blk};

//action - while shaking = true
void tap_handler(AccelAxisType axis, int32_t direction)
{
    if (timestamp==true)
    {
        timestamp=false;
#ifdef PBL_ROUND
        subImage = bitmap_layer_create(GRect(0,0,180,180));
#else
        subImage = bitmap_layer_create(GRect(0,0,144,168));
#endif
        srand(time(NULL));
        #ifdef PBL_COLOR
        subIcon = PBL_IF_ROUND_ELSE(gbitmap_create_with_resource(images_rnd[rand()%10]), gbitmap_create_with_resource(images[rand()%10]));
        #else
        subIcon = gbitmap_create_with_resource(images_blk[rand()%10]);
        #endif
        bitmap_layer_set_bitmap(subImage, subIcon);
        layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(subImage));
        void timer_callback()
        {
         gbitmap_destroy(subIcon);
         bitmap_layer_destroy(subImage);
            timestamp=true;
         };
        int millis = 1800;
        timer= app_timer_register(millis,timer_callback,NULL);
    };
}

//time change
static void timeLoop(struct tm* tick_time, TimeUnits units_changed )
{
    static char timeH[] = "00";
    static char timeM[] = "00";
    strftime(timeH, sizeof(timeH), "%H", tick_time);
    strftime(timeM, sizeof(timeM), "%M", tick_time);
    text_layer_set_text(textHours, timeH);
    text_layer_set_text(textMinutes, timeM);
}

 //most of graphical actions (background bitmap,layers for text)
void LayersCreate(void)
{
    GFont custom_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_BRUSHSCI_32));
    textHours = PBL_IF_ROUND_ELSE(text_layer_create(GRect(20, 112, 35, 35)),text_layer_create(GRect(14, 112, 35, 35)));
    textMinutes = PBL_IF_ROUND_ELSE(text_layer_create(GRect(125, 112, 35, 35)),text_layer_create(GRect(95, 112, 35, 35)));
    text_logo = PBL_IF_ROUND_ELSE(text_layer_create(GRect(40,8,100,35)),text_layer_create(GRect(23,8,100,35)));
    text_layer_set_text_color(text_logo, GColorDarkCandyAppleRed);
    text_layer_set_background_color(text_logo, GColorClear);
    text_layer_set_font(text_logo, custom_font);
    text_layer_set_text_alignment(text_logo, GTextAlignmentCenter);
    text_layer_set_text(text_logo, "Plasmids");
    text_layer_set_text_color(textHours, GColorDarkCandyAppleRed);
    text_layer_set_background_color(textHours, GColorClear);
    text_layer_set_font(textHours, custom_font);
    text_layer_set_text_alignment(textHours, GTextAlignmentCenter);
    text_layer_set_text_color(textMinutes, GColorDarkCandyAppleRed);
    text_layer_set_background_color(textMinutes, GColorClear);
    text_layer_set_font(textMinutes, custom_font);
    text_layer_set_text_alignment(textMinutes, GTextAlignmentCenter);
    layer_add_child(window_get_root_layer(window), text_layer_get_layer(text_logo));
    layer_add_child(window_get_root_layer(window), text_layer_get_layer(textHours));
    layer_add_child(window_get_root_layer(window), text_layer_get_layer(textMinutes));
    
}

//load Rect
void graph(void)
{
    Image = bitmap_layer_create(GRect(0, 0, 144, 168));
    #ifdef PBL_COLOR
    Icon = gbitmap_create_with_resource(RESOURCE_ID_evo);
    #else
    Icon = gbitmap_create_with_resource(RESOURCE_ID_evo_blk);
    #endif
    bitmap_layer_set_bitmap(Image, Icon);
    layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(Image));
    LayersCreate();
}
//load round
void graph_round(void)
{
    Image = bitmap_layer_create(GRect(0, 0, 180, 180));
    Icon = gbitmap_create_with_resource(RESOURCE_ID_evo_rd);
    bitmap_layer_set_bitmap(Image, Icon);
    layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(Image));
    LayersCreate();
}

void handle_init(void)
{
    window = window_create();
    window_stack_push(window,true);
    tick_timer_service_subscribe(MINUTE_UNIT, &timeLoop);
    accel_tap_service_subscribe(tap_handler);
#ifdef PBL_ROUND
    graph_round();
#else
    graph();
#endif
    time_t now = time(NULL);
    struct tm *current_time = localtime(&now);
    timeLoop(current_time, MINUTE_UNIT);
    
    
    
}

void handle_deinit(void)
{
    text_layer_destroy(textHours);
    text_layer_destroy(textMinutes);
    text_layer_destroy(text_logo);
    gbitmap_destroy(Icon);
    bitmap_layer_destroy(Image);
    window_destroy(window);
    accel_tap_service_unsubscribe();
    tick_timer_service_unsubscribe();
    
}

int main()
{
    handle_init();
    app_event_loop();
    handle_deinit();
    return 0;
    
}

